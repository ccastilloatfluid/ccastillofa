// Linting with cppcheck ccastillofa-12.c 
//Compiling using the "Developer Command Prompt for VS 2019"
//problem 12 - Module and Time Difference 

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(){
    char data[3];
    FILE *fp;
	fp = fopen ( "DATA.lst", "r" );        
	if (fp==NULL) return -1;

    fscanf(fp,"%3s",&data);
    while(!feof(fp)){
        int d1,h1,m1,s1,d2,h2,m2,s2,total_sg_1,total_sg_2,total_dif,day,hour,minute,seg;
        float cal_h,rest_h,cal_m,rest_m;
        
        fscanf(fp,"%3s",&data);
        d1=atof(data);
        fscanf(fp,"%3s ",&data);
        h1=atof(data);
        fscanf(fp,"%3s ",&data);
        m1=atof(data);
        fscanf(fp,"%3s ",&data);
        s1=atof(data);
        fscanf(fp,"%3s ",&data);
        d2=atof(data);
        fscanf(fp,"%3s ",&data);
        h2=atof(data);
        fscanf(fp,"%3s ",&data);
        m2=atof(data);
        fscanf(fp,"%3s ",&data);
        s2=atof(data);

        total_sg_1 = (d1*86400)+(h1*3600)+(m1*60)+s1;
        total_sg_2 = (d2*86400)+(h2*3600)+(m2*60)+s2;
        total_dif= total_sg_2 - total_sg_1;

        day= total_dif / 86400;
        cal_h= (total_dif % 86400);
        cal_h=cal_h / 3600;
        hour=(int)cal_h;
        rest_h=(cal_h - hour)*3600;
        cal_m= rest_h/60;
        minute= (int)cal_m;
        rest_m= (cal_m-minute)*60;
        seg= round(rest_m);

        printf("(%i %i %i %i) ",day,hour,minute,seg);
    }

    fclose(fp);
    system("pause");
    return(0);
}
// Running using the "Windows Command Prompt"
// assuming "ccastillofa-12.exe" and "DATA.lst" are on the same folder
// Output:
// (14 0 39 7) (17 1 16 22) (5 2 56 2) (1 17 19 24) (2 19 29 59) (7 8 37 32) (14 23 4 46) (22 16 23 12) (15 15 51 4) (0 16 13 3) (0 23 52 55)
// (10 0 11 39) (11 9 12 59) (10 15 32 10) (7 8 16 20)

